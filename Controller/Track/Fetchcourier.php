<?php

declare(strict_types=1);

namespace Dividebuy\Shipping\Controller\Track;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Constants\XmlFilePaths;
use Dividebuy\Common\Exception\InvalidParameterException;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\ResponseHelper;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Fetchcourier extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  protected ScopeConfigInterface $scopeConfig;

  protected StoreManagerInterface $storeManager;

  private ApiHelper $apiHelper;

  private ResponseHelper $responseHelper;

  /**
     * @var \Magento\Framework\Serialize\Serializer\Serialize
     */
    private $serialize;

  public function __construct(
      Context $context,
      ScopeConfigInterface $scopeConfig,
      ApiHelper $apiHelper,
      StoreManagerInterface $storeManager,
      ResponseHelper $responseHelper,
      \Magento\Framework\Serialize\Serializer\Serialize $serialize
  ) {
    $this->scopeConfig = $scopeConfig;
    $this->apiHelper = $apiHelper;
    $this->storeManager = $storeManager;
    $this->responseHelper = $responseHelper;
    $this->serialize = $serialize;

    parent::__construct($context);
  }

  /**
   * Retrieves the couriers detail.
   */
  public function execute()
  {
    $api = $this->apiHelper->getSdkApi();
    $result = $api->fetchCouriers([$this, 'processRequest']);

    return $this->responseHelper->sendJsonResponse($result);
  }

  public function processRequest()
  {
    $storeId = $this->storeManager->getStore()->getId();
    $couriers = $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_DIVIDEBUY_COURIERS,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
    $couriers = $this->serialize->unserialize($couriers);
    if (empty($couriers)) {
      throw new InvalidParameterException('DivideBuy Courier list not found', 404);
    }
    return $couriers;
  }
}
